'''
Make a Python program that asks for:
- a person's name
- a person's year of birth
- whether they were born before or after this date in that year

Based off of that information print out their name and how old they are.

'''